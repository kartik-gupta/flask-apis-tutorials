from flask import Flask,jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager

'''
Cross-Origin Resource Sharing (CORS) is an HTTP-header 
based mechanism that allows a server to indicate any other origins
(domain, scheme, or port)

'''
from flask_cors import CORS

app = Flask(__name__, instance_relative_config=True)

'''
CORS Reference Doc
https://flask-cors.readthedocs.io/en/latest/api.html

'''
CORS(app, support_credentials=True)



from flask_swagger_ui import get_swaggerui_blueprint

''' Call factory function to create our blueprint '''
SWAGGER_URL = '/API-documentaion'
API_URL = '/static/swagger.yml'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "My Application"
    }
)


''' Setting App Configuration from instance folder '''
app.config.from_pyfile('config.py')

''' Define DataBase '''
db = SQLAlchemy(app)

''' Define JWT '''
jwt = JWTManager(app)

''' Import BluePrints '''
from app.app_security.views import blueprint_user

''' Register blueprints '''
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
app.register_blueprint(blueprint_user)


@app.before_first_request
def create_tables():
    db.create_all()

# Identity is what we define when creating the access token
@jwt.additional_claims_loader
def add_claims_to_jwt(identity):
    print("Admin",app.config['ADMIN'])
    # Check if the identity is for admin or not
    if identity in app.config['ADMIN']:
        print("Admin",app.config['ADMIN'])
        return {'is_admin': True}
    return {'is_admin': False}

# This method will check if a token is blacklisted, and will be called automatically when blacklist is enabled
@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    return decrypted_token['identity'] in BLACKLIST


# Customizing jwt response/error messages.
@jwt.invalid_token_loader
def invalid_token_callback(error):
    return jsonify({
        'Message': 'Signature Verification Failed.',
        'Error': 'Invalid Token'
    }), 401


@jwt.expired_token_loader
def expired_token_callback():
    return jsonify({
        'Message': 'Relog-In Your Session Has Expired',
        'Error': 'Token Expired'
    }), 401


@jwt.unauthorized_loader
def missing_token_callback(error):
    return jsonify({
        "Message": "No Active Session Found",
        'Error': 'Authorization Required'
    }), 401

@jwt.needs_fresh_token_loader
def token_not_fresh_callback():
    return jsonify({
        "Message": "The Token Is Not Fresh.",
        'Error': 'Fresh_Token_Required'
    }), 401

@jwt.revoked_token_loader
def revoked_token_callback():
    return jsonify({
        "Message": "The Token Has Been Revoked.",
        'Error': 'Token_Revoked'
    }), 401


# @app.after_request
# def apply_caching(response):
#     response.headers["Authorization"] = "Bearer "+ app.config['ACCESS_TOKEN']
#     return response