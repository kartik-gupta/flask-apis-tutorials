class Constants:

    ''' Table Names '''
    tbl_users = "users"

    ''' Endpoints '''
    enpoint_user_get = '/v.1/fetch-user/'
    enpoint_user_post = '/v.1/create-user/'
    enpoint_user_put = '/v.1/update-user/'
    enpoint_user_delete = '/v.1/delete-user/'
    enpoint_user_login = '/v.1/login-user/'


    ORM_ExtendExisting = "extend_existing"