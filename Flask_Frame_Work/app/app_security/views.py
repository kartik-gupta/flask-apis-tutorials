from app import app,db
from flask import Blueprint, request, jsonify
from app.helper.Constants import Constants
from app.app_security.models.UsersModel import UsersModel
from werkzeug.security import generate_password_hash, check_password_hash, safe_str_cmp

 
from flask_jwt_extended.internal_utils import get_jwt_manager

from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    create_refresh_token, get_jwt_identity, get_jwt
)

# import jwt 

import datetime
from functools import wraps



''' Define Blueprint '''
blueprint_user = Blueprint('blueprint_user', __name__, url_prefix = '/user')

class UserView:


    # def token_required(f):
    #     @wraps(f)
    #     def decorated(*args, **kwargs):
    #         token = None

    #         if 'x-access-token' in request.headers:
    #             token = request.headers['x-access-token']
    #         if not token:
    #             return jsonify({"Message":'User Not Logged-In'}),401

    #         try:
    
    #             data = jwt.decode(token, app.config['SECRET_KEY'])
    #             current_user = db.session.query(UsersModel).filter(UsersModel.username == data['username']).first()
            
    #         except:
    #             return jsonify({'message':'Token is invalid'}),401
            
    #         return f(current_user, *args, **kwargs)

    #     return decorated

    
    @blueprint_user.route(Constants.enpoint_user_post, methods=['POST'])
    def create_user():
        request_data = request.get_json()
        Username = request_data.get('UserName','')
        Email = request_data.get('Email','')
        Role = request_data.get('Role','')
        Password = request_data.get('Password','')
        hashed_password = generate_password_hash(Password, method='sha256')


        if Username is not None or Email is not None or hashed_password is not None:
            Obj = UsersModel(Username.lower(),Email,hashed_password,Role.lower())
            db.session.add(Obj)
            db.session.commit()
            response = "User_ID: "+ str(Obj.user_id) +" With Username: "+ str(Obj.username)+" succesfully created"       
            return app.make_response((jsonify(response),200))
        else:
            response = "Error: Required fields are missing"
            return app.make_response((jsonify(response),500))


    @blueprint_user.route(Constants.enpoint_user_get, methods=['GET'])
    @jwt_required()
    def fetch_user():
        current_user = get_jwt_identity()
        print(current_user)
        user_id = request.args.get('User_ID',"")        
        if user_id:
            if db.session.query(UsersModel).filter(UsersModel.user_id == user_id).scalar() is not None:
                data = db.session.query(UsersModel).filter(UsersModel.user_id == user_id).one()
                response = {"UserID": data.user_id, "UserName":data.username, "Email":data.email, "Role":data.role, "Password":data.password}
                return app.make_response((jsonify(response),200))
            else:
                response = "Error: No User Exists With User ID: " + str(user_id)
                return app.make_response((jsonify(response),500))
            
        else:
            response = "Error: Required fields are missing"
            return app.make_response((jsonify(response),500))



    @blueprint_user.route(Constants.enpoint_user_put, methods=['PUT'])
    # @token_required
    @jwt_required()
    def update_user():
        request_data = request.get_json()
        UserID = request_data.get('UserId','')
        Username = request_data.get('UserName','')
        Email = request_data.get('Email','')
        Role = request_data.get('Role','')
        Password = request_data.get('Password','')
        hashed_password = generate_password_hash(Password, method='sha256')

        current_user = get_jwt_identity()
        claims = get_jwt()

        if UserID == current_user or claims['is_admin']:
            if Username is not None or Email is not None or Role is not None or UserID is not None:
                data = db.session.query(UsersModel).filter(UsersModel.user_id == UserID).one()
                data.username = Username.lower()
                data.email = Email
                data.role = Role
                data.password = hashed_password
                db.session.commit()
                response = {"Message":"Succesful","Data":{"UserID": data.user_id, "UserName":data.username, "Email":data.email, "Role":data.role, "Password":data.password}}
                return app.make_response((jsonify(response),200))
            else:
                response = "Error: Required fields are missing"
                return app.make_response((jsonify(response),500))
        else:
            response = "Error: Admin Privilege Is Required To Update Other User Details"
            return app.make_response((jsonify(response),401))



    @blueprint_user.route(Constants.enpoint_user_delete, methods=['DELETE'])
    # @token_required
    @jwt_required()
    def delete_user():

        user_id = request.args.get('User_ID',"")
        current_user = get_jwt_identity()
        claims = get_jwt()

        if user_id == current_user or claims['is_admin']:
            if user_id:
                if db.session.query(UsersModel).filter(UsersModel.user_id == user_id).scalar() is not None:
                    db.session.query(UsersModel).filter(UsersModel.user_id == user_id).delete()
                    db.session.commit()
                    response = {"Message": "Provided User Deleted"}
                    return app.make_response((jsonify(response),200))
                else:
                    response = "Error: No User Exists With User ID: " + str(user_id)
                    return app.make_response((jsonify(response),500))
            else:
                response = "Error: Required fields are missing"
                return app.make_response((jsonify(response),500))
        else:
            response = "Error: Admin Privilege Is Required"
            return app.make_response((jsonify(response),401))
        
    @blueprint_user.route(Constants.enpoint_user_login, methods=['POST'])
    def login_user():
        request_data = request.get_json()
        User_Name = request_data.get("UserName","")
        Password = request_data.get("Password","")
        

        app.config['ADMIN'] = [i.user_id for i in db.session.query(UsersModel).filter(UsersModel.role == "admin").all()]
        if User_Name is not None and Password is not None:

            if db.session.query(UsersModel).filter(UsersModel.username == User_Name).scalar() is not None:
                data = db.session.query(UsersModel).filter(UsersModel.username == User_Name.lower()).one()

                if check_password_hash(data.password, Password):
                    # token = jwt.encode({'username':data.username, 'exp':datetime.datetime.utcnow()+datetime.timedelta(minutes=5)}, app.config['SECRET_KEY'])
                    # return jsonify({'token': token.decode('UTF-8')})
                    
                    
                    access_token = create_access_token(identity=data.user_id, fresh=True) 
                    refresh_token = create_refresh_token(data.user_id)

                    return jsonify({'User':data.username, 'Message':'Copy Token Value To Authorize', 'Access_Token': 'Bearer '+ access_token,})
                    
                else:
                    response = "Error: Please enter the correct password"
                    return app.make_response((jsonify(response),401))

            else:
                response = "Error: UserName does not exist"
                return app.make_response((jsonify(response),500))
        else:
            response = "Error: Required fields are missing"
            return app.make_response((jsonify(response),500))  






        

        