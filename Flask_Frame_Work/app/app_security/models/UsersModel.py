from app import db
from app.helper.Constants import Constants


class UsersModel(db.Model):

    __bind_key__ = 'Sqlite'
    __tablename__  = Constants.tbl_users
    __table_args__ = {Constants.ORM_ExtendExisting: True}

    user_id = db.Column(db.Integer,primary_key=True, unique=True, autoincrement=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    role = db.Column(db.String(120), default='End User')
    password = db.Column(db.String(200), nullable=False )

    def __init__(self, username, email, password, role=None):
            
        self.username = username
        self.email = email
        self.password = password
        if role is not None:
            self.role = role

    def __repr__(self):
        return '<Drug %r>' % (self.username)