import os 


BASEPATH = os.environ.get("BASEPATH","/mnt/d/PythonProject/Flask_Frame_Work/app")
JWT_TOKEN_LOCATION = ['headers', 'query_string']

SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS',False)
PROPAGATE_EXCEPTIONS = os.environ.get('PROPAGATE_EXCEPTIONS', True)
JWT_BLACKLIST_ENABLED = os.environ.get('JWT_BLACKLIST_ENABLED', True) # enable blacklist feature  
DEBUG = os.environ.get("DEBUG",True)
HOSTNAME = os.environ.get("HOSTNAME",'0.0.0.0')
PORT = os.environ.get("PORT",'5000')
DBCONNECTION = os.environ.get("DBURL", '/database/tempDatabase.db')
SECRET_KEY = os.environ.get('SECRET_KEY', 'super-secret')
ADMIN = os.environ.get('ADMIN',[])
ACCESS_TOKEN = os.environ.get('ACCESS_TOKEN','')

if not DEBUG:
    raise ("Error: Debug is not defined in environment")
if HOSTNAME is None:
    raise ("Error: Hostname is not defined in environment")
if PORT is None :
    raise ("Error: Port is not defined in environment")
if DBCONNECTION is None:
    raise ("Error: DB details is not defined in environment")



SQLALCHEMY_BINDS = {
    "Sqlite": 'sqlite:///'+ BASEPATH.replace('/app','') + DBCONNECTION
}




